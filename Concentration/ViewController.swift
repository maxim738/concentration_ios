//
//  ViewController.swift
//  Concentration
//

import UIKit

class ViewController: UIViewController
{
    lazy var game = Concentration(nubmerOfPairsOfCards: (cardButtons.count+1) / 2)
    
    @IBOutlet var cardButtons: [UIButton]!
    
    @IBOutlet weak var flipCountLabel: UILabel!
    
    @IBOutlet weak var scoreValueLabel: UILabel!
    
    @IBAction func pressNewGameButton(_ sender: UIButton) {
        startNewGame()
    }
    
    @IBAction func touchCard(_ sender: UIButton){
        if let cardNumber = cardButtons.firstIndex(of: sender) {
            game.chooseCard(at: cardNumber)
            updateViewFromModel()
        } else {
            print("chosen card was not in cardButtons")
        }
       
    }
    
    func startNewGame(){
        emoji.removeAll()
        themeChoices = nil
        game.flushState() // or game = Concentration(nubmerOfPairsOfCards: (cardButtons.count+1) / 2)
        updateViewFromModel()

    }
    
    func updateViewFromModel(){
        for index in cardButtons.indices {
            let button = cardButtons[index];
            let card = game.cards[index];
            if card.isFaceUp {
                button.setTitle(emoji(for: card), for: UIControl.State.normal)
                button.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            } else {
                button.setTitle("", for: UIControl.State.normal)
                button.backgroundColor = card.isMatched ? #colorLiteral(red: 0.4488379818, green: 0.4831458998, blue: 0.9411764741, alpha: 0) : #colorLiteral(red: 1, green: 0.5763723254, blue: 0, alpha: 1)
            }
        }
        flipCountLabel.text = "Flips: \(game.flipCount)"
        scoreValueLabel.text = "Score: \(game.score)"
    }
    
    var themes = [
        ["👻","🎃","👹","😈","🍭","🦇","🍪","😰","🕯"],
        ["🇦🇺","🇦🇹","🇦🇷","🇦🇼","🇧🇪","🇬🇦","🇮🇩","🇨🇴","🇸🇴"],
        ["⌚️","📱","⏰","🖥","🖨","💿","📺","📟","🎙"],
        ["🍎","🍌","🍍","🍆","🥦","🍉","🧄","🥐","🥭"],
        ["🚗","🚎","🚕","🚚","🚜","🚛","🛺","🛵","🏍"],
        ["🐶","🐼","🐸","🙉","🐯","🐥","🐷","🐺","🦋"]
    ]
    var themeChoices : [String]?
    
    var emoji = [Int:String]()
    
    func emoji(for card: Card) -> String {
        if themeChoices == nil{ 
            themeChoices = themes[Int(arc4random_uniform(UInt32(themes.count)))]
        }
        if emoji[card.identifier] == nil, themeChoices!.count > 0{
            let randomIndex = Int(arc4random_uniform(UInt32(themeChoices!.count)))
            emoji[card.identifier] = themeChoices!.remove(at: randomIndex)
        }
        
        return emoji[card.identifier] ?? "?"
    }
    
}

