//
//  Concentration.swift
//  Concentration
//
 
import Foundation

class Concentration
{
    var cards = [Card]()
    var openedCardIds = [Int]()
    
    var indexOfFaceUpCard: Int?
    
    var flipCount = 0
    
    var score = 0
    
    func flushState(){
        for index in cards.indices{
            cards[index].isFaceUp = false;
            cards[index].isMatched = false;
        }
        indexOfFaceUpCard = nil
        flipCount = 0
        score = 0
        shuffleCards()
        openedCardIds.removeAll()
    }
    
    func shuffleCards(){
        for i in 0..<cards.count-1{
            cards.swapAt(i, Int.random(in: i..<cards.count))
        }
    }
    
    func chooseCard(at index: Int){
        if !cards[index].isMatched{
            flipCount+=1
            if let matchIndex = indexOfFaceUpCard, matchIndex != index{
                if cards[matchIndex].identifier == cards[index].identifier{
                    score += 2
                    cards[matchIndex].isMatched = true
                    cards[index].isMatched = true
                }
                else{
                    var matchOpened = false
                    var secondOpened = false
                    for i in openedCardIds.indices {
                        if openedCardIds[i] == cards[matchIndex].identifier{
                            score-=1
                            matchOpened = true
                        }
                        if openedCardIds[i] == cards[index].identifier{
                            score-=1
                            secondOpened = true
                        }
                    }
                    if !matchOpened {
                        openedCardIds.append(cards[matchIndex].identifier)
                    }
                    if !secondOpened {
                        openedCardIds.append(cards[index].identifier)
                    }
                    
                }
                indexOfFaceUpCard = nil
            }
            else{
                for flipCardIndex in cards.indices{
                    cards[flipCardIndex].isFaceUp = false
                }
                indexOfFaceUpCard = index
            }
            cards[index].isFaceUp = true
        }
    }
    
    init(nubmerOfPairsOfCards: Int){
        for _ in 1...nubmerOfPairsOfCards{
            let card = Card()
            cards += [card,card]
        }
        shuffleCards()
    }
}
